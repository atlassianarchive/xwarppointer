Format: 3.0 (quilt)
Source: xwarppointer
Binary: xwarppointer
Architecture: any
Version: 1.0-1
Maintainer: Levi Lovelock <llovelock@atlassian.com>
Homepage: http://extranet.atlassian.com/display/RELENG
Standards-Version: 3.9.2
Build-Depends: debhelper (>= 8.0.0), libx11-doc, libx11-dev
Package-List: 
 xwarppointer deb unknown extra
Checksums-Sha1: 
 61ae46e7f7f5126d86844c61e52b800ae8040993 2171 xwarppointer_1.0.orig.tar.gz
 38f45cc78663a7a9fb703fb85642bc9ffee6884e 2052 xwarppointer_1.0-1.debian.tar.gz
Checksums-Sha256: 
 b5ddacd10b55efdaf83232d4d38dfa0a8151c1aad4b09996398baeea06b02b13 2171 xwarppointer_1.0.orig.tar.gz
 bacfee68c09c68bac4633664d3db9146789a65f0d255fb4bbd714a11891b923f 2052 xwarppointer_1.0-1.debian.tar.gz
Files: 
 7103822d5c2a617740b8df2ef7df71a2 2171 xwarppointer_1.0.orig.tar.gz
 cf062e6567ababf5558799e973cfaa86 2052 xwarppointer_1.0-1.debian.tar.gz
